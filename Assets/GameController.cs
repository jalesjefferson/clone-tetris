﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameController : MonoBehaviour
{
    [Header("Cubes to Instantiate")]
    [SerializeField] List<GameObject> cubePrefabs = new List<GameObject>(7);
    [SerializeField] Transform instantiatePoint, instTrash;
    List<GameObject> piecesOnControl = new List<GameObject>();
    public int lineNumber = 33;
    public int pecaSegurando = 0;
    public int rotation = 0;
    public PieceRotationType rotationList;
    [Header("GameSpeed")]
    [SerializeField] float timeForUpdate = 0.2f;
    [SerializeField] float gameTime = 0;
    public bool canPlay = true;

    [Header("Cubes ON Scene")]
    [SerializeField] List<GameObject> pieceStoped = new List<GameObject>();
    List<GameObject> objsLixo = new List<GameObject>();

    [Header("Sounds")]
    public AudioClip acDropSound;
    public AudioClip acLineBreak;

    public float inputX, inputY;
    public float limXMin, limXMax, limYMin, limYMax;

    private void Awake()
    {
        //ConfigStageLimits();
    }

    void Start()
    {
        InstantiatePiece();
    }

    private void Update()
    {
        GetInputs();


        if (canPlay)
        {


            gameTime += Time.deltaTime;
            if (gameTime >= timeForUpdate)
            {
                MovePieceDown();
                gameTime = 0;
            }
        }
    }

    private void GetInputs()
    {
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");
        if (Input.GetButtonDown("Jump"))
           MakePieceRotate(rotationList.RotationList(pecaSegurando, rotation, piecesOnControl.ToArray()));
    }

    private void MovePieceDown()
    {
        if (CheckFuturePos(0, -1))
            MovePiece(0, -1);
        else
            BlockActualPiece();
    }

    private void MovePieceInput(float x)
    {
        if (x != 0)
        {
            if (x > 0 && CheckFuturePos(1,0))
                MovePiece(1,0);
            else if (x < 0 && CheckFuturePos(-1, 0))
                MovePiece(-1,0);
        }
    }

    private void MovePiece(int dirX, int dirY)
    {
        foreach (GameObject gb in piecesOnControl)
        {
            gb.transform.position += new Vector3(dirX,dirY,0);
        }
    }

    private void InstantiatePiece()
    {
        if(piecesOnControl.Count != 0)
        {
            foreach (GameObject gb in piecesOnControl)
            {
                pieceStoped.Add(gb);
            }
            piecesOnControl.RemoveRange(0, 4);
            rotation = 0;
            CheckLine();
        }

        pecaSegurando = UnityEngine.Random.Range(0, 7);
        SimpleInstantiate(pecaSegurando);
        canPlay = true;
    }

    private void SimpleInstantiate(int value)
    {
        for (int i = 0; i < 4; i++)
        {
            GameObject temp = Instantiate(cubePrefabs[value], instantiatePoint.position, Quaternion.identity, instTrash.transform);
            piecesOnControl.Add(temp);
        }

        switch (value)
        {
            case (0):
                OrganizeCubo();
                break;
            case (1):
                OrganizeLongo();
                break;
            case (2):
                OrganizeCavalo(-1);
                break;
            case (3):
                OrganizeCavalo(1);
                break;
            case (4):
                OrganizeSerpente(-1);
                break;
            case (5):
                OrganizeSerpente(1);
                break;
            case (6):
                OrganizeTPiece();
                break;
        }
    }

    #region Check if a piece can move or rotate

    private bool CheckFuturePos(Vector2[] posFutura)
    {
        for (int i = 0; i < posFutura.Length; i++)
        {
            if (posFutura[i].x < limXMin || posFutura[i].x > limXMax || posFutura[i].y < limYMin)
                return false;
            else
            {
                foreach (GameObject go in pieceStoped)
                {
                    if (posFutura[i].x == go.transform.position.x && posFutura[i].y == go.transform.position.y)
                        return false;
                }
            }
        }
        return true;
    }

    private bool CheckFuturePos(int dirX, int dirY)
    {
        Vector2[] posFutura = new Vector2[4];

        for (int i = 0; i < posFutura.Length; i++)
        {
            posFutura[i] = piecesOnControl[i].transform.position + new Vector3(dirX, dirY, 0);
        }

        return CheckFuturePos(posFutura);
    }

    private void CheckLine()
    {
        int linesExplode = 0;

        for (int i = 0; i < lineNumber; i++)
        {
            int count = 0;

            foreach (GameObject go in pieceStoped)
            {
                if (go.transform.position.y == i)
                    count++;   
            }

            if(count >= 10)
            {
                linesExplode++;
                foreach (GameObject go in pieceStoped)
                {
                    if (go.transform.position.y == i)
                        objsLixo.Add(go);
                }
                foreach (GameObject go in objsLixo)
                {
                    Destroy(go);
                    pieceStoped.Remove(go);
                }

                foreach (GameObject go in pieceStoped)
                {
                    if (go.transform.position.y > i)
                        go.transform.position += new Vector3(0, -1, 0);
                }

                objsLixo.RemoveRange(0, objsLixo.Count);
                count = 0;
                i--;
            }
        }

        if (linesExplode > 0)
        {
            AudioSource.PlayClipAtPoint(acLineBreak, Camera.main.transform.position);
        }
    }

    private void BlockActualPiece()
    {
        if (!CheckEndGame())
            Application.LoadLevel(0);

        canPlay = false;
        AudioSource.PlayClipAtPoint(acDropSound, Camera.main.transform.position);

        InstantiatePiece();
    }

    private bool CheckEndGame()
    {
        foreach(GameObject gb in piecesOnControl)
        {
            if (gb.transform.position.y + 1 >= limYMax)
            {
                return false;
            }
        }

        return true;
    }

    private void ConfigStageLimits()
    {
        Camera cam = Camera.main;
        limXMin = cam.ViewportToWorldPoint(Vector3.zero).x;
        limXMax = cam.ViewportToWorldPoint(Vector3.one).x;
        limYMin = cam.ViewportToWorldPoint(Vector3.zero).y;
        limYMax = cam.ViewportToWorldPoint(Vector3.one).y;
    }

    #endregion

    #region Configuration of each block type

    private void OrganizeCubo()
    {
        piecesOnControl[1].transform.position += new Vector3(1, 0, 0);
        piecesOnControl[2].transform.position += new Vector3(0, 1, 0);
        piecesOnControl[3].transform.position += new Vector3(1, 1, 0);
    }

    private void OrganizeLongo()
    {
        for (int i = 0; i < 4; i++)
        {
            piecesOnControl[i].transform.position += new Vector3(0, i, 0);
        }
    }

    private void OrganizeCavalo(int type)
    {
        piecesOnControl[1].transform.position += new Vector3(0, 1, 0);
        piecesOnControl[2].transform.position += new Vector3(type, 1, 0);
        piecesOnControl[3].transform.position += new Vector3(type*2, 1, 0);
    }

    private void OrganizeSerpente(int type)
    {
        if (type < 0)
        {
            piecesOnControl[0].transform.position += new Vector3(0, 1,0);
            piecesOnControl[1].transform.position += new Vector3(1, 1, 0);
            piecesOnControl[2].transform.position += new Vector3(1, 0, 0);
            piecesOnControl[3].transform.position += new Vector3(2, 0, 0);
        }
        else
        {
            piecesOnControl[1].transform.position += new Vector3(1, 0, 0);
            piecesOnControl[2].transform.position += new Vector3(1, 1, 0);
            piecesOnControl[3].transform.position += new Vector3(2, 1, 0);
        }
    }

    private void OrganizeTPiece()
    {
        piecesOnControl[1].transform.position += new Vector3(1, 0, 0);
        piecesOnControl[2].transform.position += new Vector3(2, 0, 0);
        piecesOnControl[3].transform.position += new Vector3(1, 1, 0);
    }

    #endregion

    #region BlockRotation

    private void MakePieceRotate(Vector2[] posFutura)
    {
        if (CheckFuturePos(posFutura))
        {
            rotation++;
            for (int i = 0; i < 4; i++)
            {
                piecesOnControl[i].transform.position = posFutura[i];
            }
        }

        if (rotation > 3)
            rotation = 0;
    }

    #endregion
}
