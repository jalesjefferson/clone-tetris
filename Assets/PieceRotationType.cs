﻿using UnityEngine;

public class PieceRotationType : MonoBehaviour
{
    public Vector2[] RotationList(int wichPiece, int rotation, GameObject[] posAtual)
    {
        Vector2[] posFutura = new Vector2[4];

        switch (wichPiece) {
            // Peca longa
            case (1):
                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(-2, 1);
                        posFutura[1] = new Vector2(-1, 0);
                        posFutura[2] = new Vector2(0, -1);
                        posFutura[3] = new Vector2(1, -2);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(1, 2);
                        posFutura[1] = new Vector2(0, 1);
                        posFutura[2] = new Vector2(-1, 0);
                        posFutura[3] = new Vector2(-2, -1);
                        break;
                    case (2):
                        posFutura[0] = new Vector2(2, -1);
                        posFutura[1] = new Vector2(1, 0);
                        posFutura[2] = new Vector2(0, 1);
                        posFutura[3] = new Vector2(-1, 2);
                        break;
                    case (3):
                        posFutura[0] = new Vector2(-1, -2);
                        posFutura[1] = new Vector2(0, -1);
                        posFutura[2] = new Vector2(1, 0);
                        posFutura[3] = new Vector2(2, 1);
                        break;
                }
                break;

            // Peca J
            case (2):
                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(-2, 0);
                        posFutura[1] = new Vector2(-1, -1);
                        posFutura[3] = new Vector2(1, 1);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(0, 2);
                        posFutura[1] = new Vector2(-1, 1);
                        posFutura[3] = new Vector2(1, -1);
                        break;
                    case (2):
                        posFutura[0] = new Vector2(2,0);
                        posFutura[1] = new Vector2(1, 1);
                        posFutura[3] = new Vector2(-1, -1);
                        break;
                    case (3):
                        posFutura[0] = new Vector2(0,-2);
                        posFutura[1] = new Vector2(1, -1);
                        posFutura[3] = new Vector2(-1, 1);
                        break;
                }
                break;

            // Peca L
            case (3):
                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(0, 2);
                        posFutura[1] = new Vector2(1, 1);
                        posFutura[3] = new Vector2(-1, -1);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(2, 0);
                        posFutura[1] = new Vector2(1, -1);
                        posFutura[3] = new Vector2(-1, 1);
                        break;
                    case (2):
                        posFutura[0] = new Vector2(0, -2);
                        posFutura[1] = new Vector2(-1, -1);
                        posFutura[3] = new Vector2(1, 1);
                        break;
                    case (3):
                        posFutura[0] = new Vector2(-2, 0);
                        posFutura[1] = new Vector2(-1, 1);
                        posFutura[3] = new Vector2(1, -1);
                        break;
                }
                break;

            // Peca Z
            case (4):
                rotation = rotation % 2;

                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(1, 1);
                        posFutura[2] = new Vector2(-1, 1);
                        posFutura[3] = new Vector2(-2, 0);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(-1, -1);
                        posFutura[2] = new Vector2(1, -1);
                        posFutura[3] = new Vector2(2, 0);
                        break;
                }
                break;

            // Peca S
            case (5):
                rotation = rotation % 2;

                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(2, 0);
                        posFutura[1] = new Vector2(1, 1);
                        posFutura[3] = new Vector2(-1, 1);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(-2, 0);
                        posFutura[1] = new Vector2(-1, -1);
                        posFutura[3] = new Vector2(1, -1);
                        break;
                }
                break;

            // Peca T
            case (6):
                switch (rotation)
                {
                    case (0):
                        posFutura[0] = new Vector2(1,1);
                        posFutura[2] = new Vector2(-1, -1);
                        posFutura[3] = new Vector2(1, -1);
                        break;
                    case (1):
                        posFutura[0] = new Vector2(1, -1);
                        posFutura[2] = new Vector2(-1, 1);
                        posFutura[3] = new Vector2(-1, -1);
                        break;
                    case (2):
                        posFutura[0] = new Vector2(-1, -1);
                        posFutura[2] = new Vector2(1, 1);
                        posFutura[3] = new Vector2(-1, 1);
                        break;
                    case (3):
                        posFutura[0] = new Vector2(-1, 1);
                        posFutura[2] = new Vector2(1, -1);
                        posFutura[3] = new Vector2(1, 1);
                        break;
                }
                break;
        }

        posFutura = ConfigFuretePos(posAtual, posFutura);
        return posFutura;
    }

    public Vector2[] ConfigFuretePos(GameObject[] objs,Vector2[] dir)
    {
        Vector2[] furutePos = new Vector2[4];

        furutePos[0] = objs[0].transform.position + new Vector3(dir[0].x, dir[0].y,0);
        furutePos[1] = objs[1].transform.position + new Vector3(dir[1].x, dir[1].y, 0);
        furutePos[2] = objs[2].transform.position + new Vector3(dir[2].x, dir[2].y, 0);
        furutePos[3] = objs[3].transform.position + new Vector3(dir[3].x, dir[3].y, 0);

        return furutePos;
    }
}
